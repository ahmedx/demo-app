<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Student;
use Faker\Generator as Faker;

$factory->define(Student::class, function (Faker $faker) {
    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'email' => $faker->unique()->safeEmail,
        'address' => $faker->address,
        'nic' => $faker->randomNumber($nbDigits = null, $strict = false),
        'passport' => $faker->randomNumber($nbDigits = null, $strict = false),
        'date_of_birth' => $faker->date($format = 'Y-m-d', $max = 'now'),
        'phone' => $faker->randomNumber($nbDigits = null, $strict = false),
        'gender' => $faker->randomElement($array = array ('male','female')),
        'created_at' => now(),
        'updated_at' => now()
    ];
});
