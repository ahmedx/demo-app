<?php

namespace App\Repositories;


use App\Student;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class StudentRepository implements StudentRepositoryInterface
{

    /**
     * Get's all students.
     *
     * @return mixed
     */
    public function all()
    {
        return Student::orderBy('id', 'DESC')->paginate(10);
    }

    /**
     * Creates a new Student
     * @param $data
     * @throws \Exception
     */
    public function create($data)
    {
        DB::beginTransaction();
        try {
            if (array_key_exists('image', $data)) {
                $data['image'] = now()->timestamp . '_' . uniqid() . '.' . request()->image->getClientOriginalExtension();
                request()->image->storeAs('public/images', $data['image']);
            }
            Student::create($data);
        } catch (\Exception $e) {
            DB::rollBack();
            if ($data['image'] !== null) {
                Storage::delete('public/images/' . $data['image']);
            }
            throw $e;
        }
        DB::commit();
    }

    /**
     * Get's a student by it's ID
     *
     * @param int
     * @return \App\Student
     */
    public function get($student_id)
    {
        return Student::findOrFail($student_id);
    }

    /**
     * Deletes a student.
     *
     * @param int
     * @throws
     */
    public function delete($student_id)
    {
        $student = $this->get($student_id);

        $image = $student->image;
        try {
            if ($image !== null) {
                Storage::delete('public/images/' . $image);
            }
            $student->delete();
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Updates a post.
     *
     * @param int
     * @param array
     * @throws
     */
    public function update($student_id, $data)
    {
        $student = $this->get($student_id);

        $image = $student->image;

        DB::beginTransaction();
        try {
            if (array_key_exists('image', $data)) {
                $data['image'] = now()->timestamp . '_' . uniqid() . '.' . request()->image->getClientOriginalExtension();
                request()->image->storeAs('public/images', $data['image']);
            }
            $student->update($data);
        } catch (\Exception $e) {
            DB::rollBack();
            if ($data['image'] !== null) {
                Storage::delete('public/images/' . $data['image']);
            }
            throw $e;
        }
        // Delete Old Image
        if ($image !== null) {
            Storage::delete('public/images/' . $image);
        }
        DB::commit();
    }

}