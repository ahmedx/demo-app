<?php

namespace App\Repositories;


interface StudentRepositoryInterface
{
    /**
     * Get's all students.
     *
     * @return mixed
     */
    public function all();

    /**
     * Creates a new Student
     *
     */
    public function create($data);

    /**
     * Get's a student by it's ID
     *
     * @param int
     */
    public function get($student_id);


    /**
     * Deletes a student.
     *
     * @param int
     */
    public function delete($student_id);

    /**
     * Updates a post.
     *
     * @param int
     * @param array
     */
    public function update($student_id, $data);
}