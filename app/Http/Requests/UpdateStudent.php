<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateStudent extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('student');

        return [
            'first_name' => ['required', 'string'],
            'last_name' => ['required', 'string'],
            'email' => ['required', 'email', 'unique:students,email,' . $id],
            'address' => ['required', 'string'],
            'nic' => ['required_without:passport', 'nullable', 'string'],
            'passport' => ['required_without:nic', 'nullable', 'string'],
            'date_of_birth' => ['required', 'date'],
            'phone' => ['required', 'integer'],
            'gender' => ['required', 'string'],
            'image' => ['nullable', 'image']
        ];
    }
}
