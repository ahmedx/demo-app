<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreStudent;
use App\Http\Requests\UpdateStudent;
use App\Repositories\StudentRepositoryInterface;

class StudentsController extends Controller
{
    /**
     * @var StudentRepositoryInterface
     */
    private $student;

    /**
     * StudentsController constructor.
     * @param StudentRepositoryInterface $student
     */
    public function __construct(StudentRepositoryInterface $student)
    {
        $this->student = $student;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $students = $this->student->all();

        return view('students.index', compact('students'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('students.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreStudent
     * @return \Illuminate\Http\Response
     * @throws
     */
    public function store(StoreStudent $request)
    {
        $attributes = $request->validated();

        $this->student->create($attributes);

        return back()->with('success', 'Student Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  $student
     * @return \Illuminate\Http\Response
     */
    public function show($student)
    {
        $student = $this->student->get($student);

        return view('students.show')->with(compact('student'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  $student
     * @return \Illuminate\Http\Response
     */
    public function edit($student)
    {
        $student = $this->student->get($student);

        return view('students.edit')->with(compact('student'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateStudent $request
     * @param  $student
     * @return \Illuminate\Http\Response
     * @throws
     */
    public function update(UpdateStudent $request, $student)
    {
        $attributes = $request->validated();

        $this->student->update($student, $attributes);

        return back()->with('success', 'Student Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $student
     * @return \Illuminate\Http\Response
     * @throws
     */
    public function destroy($student)
    {
        $this->student->delete($student);

        return back()->with('success', 'Student Deleted');
    }
}
