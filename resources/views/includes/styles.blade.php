@section('styles')
    <style>
        header, main, footer {
            padding-left: 300px !important;
        }

        @media only screen and (max-width : 992px) {
            header, main, footer {
                padding-left: 0 !important;
            }
        }
    </style>
@endsection