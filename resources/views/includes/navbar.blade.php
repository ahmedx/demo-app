<nav>
    <div class="container">
        <div class="nav-wrapper">
            <a href="{{ url('/')  }}" class="brand-logo"><div class="hide-on-small-and-down">{{ config('app.name', 'UMS') }}</div></a>
            <a href="#" data-target="mobile-nav" class="sidenav-trigger"><i class="material-icons">menu</i></a>
            <ul class="right hide-on-med-and-down">
                <!-- Authentication Links -->
                @guest
                    <li class="{{ Route::currentRouteName() == 'login' ? 'active' : '' }}">
                        <a href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                    @if (Route::has('register'))
                        <li class="{{ Route::currentRouteName() == 'register' ? 'active' : '' }}">
                            <a href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                    @endif
                @else
                    <a href="{{ route('logout') }}"
                       class="btn"
                       onclick="event.preventDefault();
                               document.getElementById('logout-form').submit();">{{ __('Logout') }}</a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                @endguest
            </ul>
        </div>
    </div>
</nav>

<ul class="sidenav" id="mobile-nav">
    @guest
        <li class="{{ Route::currentRouteName() == 'login' ? 'active' : '' }}">
            <a href="{{ route('login') }}">{{ __('Login') }}</a>
        </li>
        @if (Route::has('register'))
            <li class="{{ Route::currentRouteName() == 'register' ? 'active' : '' }}">
                <a href="{{ route('register') }}">{{ __('Register') }}</a>
            </li>
        @endif
    @else
        <li>
            <div class="user-view">
                <div class="background blue darken-3">
                    {{--<img src="https://source.unsplash.com/random">--}}
                </div>
                <a href="javascript:void(0)">
                    {{--<img class="circle" src="https://source.unsplash.com/random">--}}
                </a>
                <a href="javascript:void(0)">
                    <span class="white-text name">{{ Auth::user()->name }}</span>
                </a>
                <a href="javascript:void(0)">
                    <span class="white-text email">{{ Auth::user()->email }}</span>
                </a>
            </div>
        </li>
        <li>
            <a class="subheader">Student Management</a>
        </li>
        <li>
            <div class="divider"></div>
        </li>
        <li>
            <a class="waves-effect" href="{{ route('dashboard') }}">
                Dashboard <i class="material-icons">dashboard</i>
            </a>
        </li>
        <li>
            <a class="waves-effect" href="{{ route('students.index') }}">
                All Students <i class="material-icons">people</i>
            </a>
        </li>
        <li>
            <a class="waves-effect" href="{{ route('students.create') }}">
                Student Registration <i class="material-icons">person_add</i>
            </a>
        </li>
        <li>
            <a class="waves-effect" href="{{ route('users.index') }}">
                Users <i class="material-icons">supervised_user_circle</i>
            </a>
        </li>
        <li>
            <div href="#!" class="divider"></div>
        </li>
        <li>
            <a href="{{ route('logout') }}"
               onclick="event.preventDefault();
                               document.getElementById('logout-form').submit();">{{ __('Logout') }}</a>
        </li>
    @endguest
</ul>