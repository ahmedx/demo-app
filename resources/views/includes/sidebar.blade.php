<ul id="slide-out" class="sidenav sidenav-fixed">
    <li>
        <div class="user-view">
            <div class="background blue darken-3">
                {{--<img src="https://source.unsplash.com/random">--}}
            </div>
            <a href="javascript:void(0)">
                {{--<img class="circle" src="https://source.unsplash.com/random">--}}
            </a>
            <a href="javascript:void(0)">
                <span class="white-text name">{{ Auth::user()->name }}</span>
            </a>
            <a href="javascript:void(0)">
                <span class="white-text email">{{ Auth::user()->email }}</span>
            </a>
        </div>
    </li>
    <li>
        <a class="subheader">Student Management</a>
    </li>
    <li>
        <div class="divider"></div>
    </li>
    <li>
        <a class="waves-effect" href="{{ route('dashboard') }}">
            Dashboard <i class="material-icons">dashboard</i>
        </a>
    </li>
    <li>
        <a class="waves-effect" href="{{ route('students.index') }}">
            Students <i class="material-icons">people</i>
        </a>
    </li>
    <li>
        <a class="waves-effect" href="{{ route('students.create') }}">
            Student Registration <i class="material-icons">person_add</i>
        </a>
    </li>
    <li>
        <a class="waves-effect" href="{{ route('users.index') }}">
            Users <i class="material-icons">supervised_user_circle</i>
        </a>
    </li>
</ul>