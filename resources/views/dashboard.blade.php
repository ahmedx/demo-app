@extends('layouts.app')

@include('includes/styles')

@section('content')
    @include('includes/sidebar')

    <div class="container">
        <div class="row">
            <div class="col s12 m3">
                <div class="card z-depth-3 red darken-1 white-text">
                    <div class="card-content">
                        <div class="card-title center">Registered Students</div>
                        <div class="center">
                            <i class="material-icons large">people</i>
                        </div>
                        <div class="center">
                            <h5>{{$students}}</h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col s12 m3">
                <div class="card z-depth-3 purple darken-1 white-text">
                    <div class="card-content">
                        <div class="card-title center">Registered Users</div>
                        <div class="center">
                            <i class="material-icons large">supervised_user_circle</i>
                        </div>
                        <div class="center">
                            <h5>{{$users}}</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="fixed-action-btn">
        <a class="btn-floating btn-large waves-effect waves-light red pulse" href="{{ route('students.create') }}">
            <i class="large material-icons">add</i>
        </a>
    </div>
@endsection
