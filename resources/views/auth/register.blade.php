@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col s12 m8 offset-m2">
                <div class="card z-depth-4">
                    <div class="card-content">
                        <div class="card-title">{{ __('Register') }}</div>
                        <form method="POST" action="{{ route('register') }}">
                            @csrf

                            <div class="row">
                                <div class="input-field col s12">
                                    <label for="name">{{ __('Name') }}</label>
                                    <input id="name" type="text" class="@error('name') invalid @enderror"
                                           name="name" value="{{ old('name') }}" autocomplete="name" autofocus required>

                                    @error('name')
                                    <span class="helper-text red-text">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row">
                                <div class="input-field col s12">
                                    <label for="email">{{ __('E-Mail Address') }}</label>
                                    <input id="email" type="email" class="@error('email') invalid @enderror"
                                           name="email" value="{{ old('email') }}" autocomplete="email" required>

                                    @error('email')
                                    <span class="helper-text red-text">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row">
                                <div class="input-field col s12">
                                    <label for="password">{{ __('Password') }}</label>
                                    <input id="password" type="password"
                                           class="@error('password') invalid @enderror" name="password"
                                           autocomplete="new-password" required>

                                    @error('password')
                                    <span class="helper-text red-text">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row">
                                <div class="input-field col s12">
                                    <label for="password-confirm">{{ __('Confirm Password') }}</label>
                                    <input id="password-confirm" type="password" class="form-control"
                                           name="password_confirmation" autocomplete="new-password" required>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col s12">
                                    <button type="submit" class="btn waves-effect waves-light block-on-small-and-down">
                                        {{ __('Register') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
