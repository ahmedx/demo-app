@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col s12 m8 offset-m2">
                <div class="card z-depth-4">
                    <div class="card-content">
                        <div class="card-title">{{ __('Login') }}</div>
                        <form method="POST" action="{{ route('login') }}">
                            @csrf

                            <div class="row">
                                <div class="input-field col s12">
                                    <label for="email">{{ __('E-Mail Address') }}</label>
                                    <input id="email" type="email"
                                           class="@error('email') invalid @enderror"
                                           name="email" value="{{ old('email') }}" autocomplete="email" required
                                           autofocus>

                                    @error('email')
                                    <span class="helper-text red-text">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row">
                                <div class="input-field col s12">
                                    <label for="password">{{ __('Password') }}</label>
                                    <input id="password" type="password"
                                           class="@error('password') invalid @enderror" name="password" required
                                           autocomplete="current-password">

                                    @error('password')
                                    <span class="helper-text red-text">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row">
                                <div class="input-field col s12">
                                    <p>
                                        <label>
                                            <input type="checkbox" name="remember"
                                                   id="remember" {{ old('remember') ? 'checked' : '' }} />
                                            <span>{{ __('Remember Me') }}</span>
                                        </label>
                                    </p>
                                </div>
                            </div>

                            <div class="row center">
                                <div class="col s12" id="login-actions">
                                    <button type="submit" class="btn waves-effect waves-light block-on-small-and-down">
                                        {{ __('Login') }}
                                    </button>

                                    @if (Route::has('password.request'))
                                        <a class="btn waves-effect waves-light block-on-small-and-down"
                                           href="{{ route('password.request') }}">
                                            {{ __('Forgot Your Password?') }}
                                        </a>
                                    @endif
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
