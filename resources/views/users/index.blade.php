@extends('layouts.app')

@include('includes/styles')

@section('content')
    @include('includes/sidebar')

    <h3 class="center">Users</h3>
    <hr>
    <table class="striped highlight centered responsive-table mb-1">
        <thead>
        <tr>
            <th width="100">ID</th>
            <th>Name</th>
            <th>Email</th>
        </tr>
        </thead>

        <tbody>
        @foreach ($users as $user)
            <tr>
                <td>{{ $user->id }}</td>
                <td>{{ $user->name }}</td>
                <td>{{ $user->email }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {{$users->links()}}
@endsection