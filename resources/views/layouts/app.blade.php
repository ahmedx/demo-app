<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    
    <!-- Styles -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

    @yield('styles')
</head>
<body>
<main class="py-4">
    @include('includes/navbar')
    <div>
        @yield('content')
    </div>
</main>

<footer class="page-footer">
    <div class="footer-copyright">
        <div class="container">
            &copy; {{ now()->year }} Copyrights
            <a class="grey-text text-lighten-4 right" href="#!">Help</a>
        </div>
    </div>
</footer>
<!-- Scripts -->
<script src="{{ mix('js/app.js') }}" defer></script>
</body>
</html>
