@extends('layouts.app')

@include('includes/styles')

@section('content')
    @include('includes/sidebar')
    <div class="container">
        <div class="row">
            <div class="col s12">
                <h3 class="center">Student Registration</h3>
            </div>
        </div>
        @if (session('success'))
            <script>
                var message = @json(session('success'));
                document.addEventListener('DOMContentLoaded', function () {
                    window.M.toast({
                        html: message
                    })
                })
            </script>
        @endif

        <div class="card">
            <div class="card-content">
                <form action="{{ route('students.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf()

                    <div class="row">
                        <div class="input-field col s12 m6">
                            <label for="first_name">First Name</label>
                            <input type="text" id="first_name"
                                   class="@error('first_name') invalid @enderror"
                                   name="first_name" value="{{ old('first_name') }}">

                            @error('first_name')
                            <span class="helper-text red-text">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="input-field col s12 m6">
                            <label for="last_name">Last Name</label>
                            <input id="last_name" type="text"
                                   class="@error('last_name') invalid @enderror"
                                   name="last_name" value="{{ old('last_name') }}">

                            @error('last_name')
                            <span class="helper-text red-text">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12 m6">
                            <label for="email">Email</label>
                            <input type="email" id="email"
                                   class="@error('email') invalid @enderror"
                                   name="email" value="{{ old('email') }}">

                            @error('email')
                            <span class="helper-text red-text">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="input-field col s12 m6">
                            <label for="address">Address</label>
                            <textarea id="address"
                                      class="materialize-textarea @error('address') invalid @enderror"
                                      name="address">{{ old('address') }}</textarea>

                            @error('address')
                            <span class="helper-text red-text">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12 m6">
                            <label for="nic">NIC</label>
                            <input type="text" id="nic"
                                   class="@error('nic') invalid @enderror"
                                   name="nic" value="{{ old('nic') }}">

                            @error('nic')
                            <span class="helper-text red-text">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="input-field col s12 m6">
                            <label for="passport">Passport No</label>
                            <input id="passport" type="text"
                                   class="@error('passport') invalid @enderror"
                                   name="passport" value="{{ old('passport') }}">

                            @error('passport')
                            <span class="helper-text red-text">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12 m6">
                            <label for="date_of_birth">Date of Birth</label>
                            <input type="text" id="date_of_birth"
                                   class="datepicker @error('date_of_birth') invalid @enderror"
                                   name="date_of_birth" value="{{ old('date_of_birth') }}">

                            @error('date_of_birth')
                            <span class="helper-text red-text">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="input-field col s12 m6">
                            <label for="phone">Phone</label>
                            <input id="phone" type="number"
                                   class="@error('phone') invalid @enderror"
                                   name="phone" value="{{ old('phone') }}">

                            @error('phone')
                            <span class="helper-text red-text">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12 m6">
                        <span>
                            <label>
                                <input name="gender" type="radio" value="male" checked/>
                                <span>Male</span>
                            </label>
                        </span>
                            <span>
                            <label>
                                <input name="gender" type="radio" value="female"/>
                                <span>Female</span>
                            </label>
                        </span>

                            @error('gender')
                            <span class="helper-text red-text">{{ $message }}</span>
                            @enderror
                        </div>

                        <div class="file-field input-field col s12 m6">
                            <div class="btn">
                                <span>File</span>
                                <input type="file" name="image">
                            </div>
                            <div class="file-path-wrapper">
                                <input class="file-path validate" type="text">
                            </div>

                            @error('image')
                            <span class="helper-text red-text">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <button class="btn waves-effect waves-light block-on-small-and-down mb-1" type="submit"
                            name="action">
                        Submit
                        <i class="material-icons right">send</i>
                    </button>
                    <button class="btn waves-effect waves-light block-on-small-and-down mb-1" type="reset">
                        Clear
                        <i class="material-icons right">clear</i>
                    </button>
                </form>
            </div>
        </div>
    </div>
@endsection
