@extends('layouts.app')

@include('includes/styles')

@section('content')
    @include('includes/sidebar')
    <div class="container">
        <h3 class="center">Student ID - {{ $student->id }}</h3>
        <div class="card">
            <div class="card-content">
                @if($student->image !== null)
                    <div class="row">
                        <div class="col s12 m6 offset-m3">
                            <img class="materialboxed responsive-img" src="{{$student->image_path}}"
                                 alt="{{ $student->full_name }}">
                        </div>
                    </div>
                @endif
                <div class="row">
                    <div class="input-field col s12 m6">
                        <label for="first_name">First Name</label>
                        <input type="text" id="first_name"
                               name="first_name" value="{{ $student->first_name }}" disabled>
                    </div>
                    <div class="input-field col s12 m6">
                        <label for="last_name">Last Name</label>
                        <input id="last_name" type="text"
                               name="last_name" value="{{ $student->last_name }}" disabled>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12 m6">
                        <label for="email">Email</label>
                        <input type="email" id="email"
                               name="email" value="{{ $student->email }}" disabled>
                    </div>
                    <div class="input-field col s12 m6">
                        <label for="address">Address</label>
                        <textarea id="address"
                                  class="materialize-textarea"
                                  name="address" disabled>{{ $student->address }}</textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12 m6">
                        <label for="nic">NIC</label>
                        <input type="text" id="nic"
                               name="nic" value="{{ $student->nic }}" disabled>
                    </div>
                    <div class="input-field col s12 m6">
                        <label for="passport">Passport No</label>
                        <input id="passport" type="text"
                               name="passport" value="{{ $student->passport }}" disabled>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12 m6">
                        <label for="date_of_birth">Date of Birth</label>
                        <input type="text" id="date_of_birth"
                               class="datepicker"
                               name="date_of_birth" value="{{ $student->date_of_birth }}" disabled>
                    </div>
                    <div class="input-field col s12 m6">
                        <label for="phone">Phone</label>
                        <input id="phone" type="text"
                               name="phone" value="{{ $student->phone }}" disabled>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12 m6">
                        <span>
                            <label>
                                <input name="gender" type="radio" {{ $student->gender == 'male' ? 'checked' : '' }} disabled/>
                                <span>Male</span>
                            </label>
                        </span>
                                    <span>
                            <label>
                                <input name="gender" type="radio" {{ $student->gender == 'female' ? 'checked' : '' }} disabled/>
                                <span>Female</span>
                            </label>
                        </span>
                    </div>
                    <div class="input-field col s12 m6">
                        <label for="age">Age</label>
                        <input type="text" id="age"
                               class="datepicker"
                               name="age" value="{{ $student->age }}" disabled>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
