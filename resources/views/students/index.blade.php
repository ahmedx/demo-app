@extends('layouts.app')

@include('includes/styles')

@section('content')
    @include('includes/sidebar')

    <h3 class="center">Students</h3>
    <hr>
    @if (session('success'))
        <script>
            var message = @json(session('success'));
            document.addEventListener('DOMContentLoaded', function () {
                window.M.toast({
                    html: message
                })
            })
        </script>
    @endif

    <table class="striped highlight centered responsive-table mb-1">
        <thead>
        <tr>
            <th width="100">ID</th>
            <th>Fullname</th>
            <th>Email</th>
            <th>Address</th>
            <th>Nic</th>
            <th>Passport</th>
            <th>DOB</th>
            <th>Age</th>
            <th>Phone</th>
            <th>Gender</th>
            <th width="150" class="center">Actions</th>
        </tr>
        </thead>

        <tbody>
        @forelse ($students as $student)
            <tr>
                <td>{{ $student->id }}</td>
                <td>{{ $student->fullname }}</td>
                <td>{{ $student->email }}</td>
                <td>{{ $student->address }}</td>
                <td>{{ $student->nic }}</td>
                <td>{{ $student->passport }}</td>
                <td>{{ $student->date_of_birth }}</td>
                <td>{{ $student->age }}</td>
                <td>{{ $student->phone }}</td>
                <td style="text-transform: capitalize">{{ $student->gender }}</td>
                <td>
                    <a class="btn-floating btn-small blue waves-effect waves-light tooltipped"
                       data-position="top" data-tooltip="View"
                       href="{{ route('students.show', $student->id) }}"><i class="material-icons">visibility</i></a>
                    <a class="btn-floating btn-small orange waves-effect waves-light tooltipped"
                       data-position="top" data-tooltip="Edit"
                       href="{{ route('students.edit', $student->id) }}"><i class="material-icons">edit</i></a>
                    <form action="{{ route('students.destroy', $student->id) }}" method="POST"
                          style="display: inline-block;">
                        @csrf
                        @method('DELETE')
                        <button class="btn-floating btn-small red waves-effect waves-light tooltipped"
                                data-position="left" data-tooltip="Delete" type="submit">
                            <i class="material-icons">delete</i></button>
                    </form>
                </td>
            </tr>
        @empty
            <tr>
                <td>No students</td>
            </tr>
        @endforelse
        </tbody>
    </table>
    {{$students->links()}}
@endsection