import M from 'materialize-css/dist/js/materialize.js'

window.M = M;

document.addEventListener('DOMContentLoaded', function () {
    let sidenav = document.querySelectorAll('.sidenav');
    let sidenavInstance = M.Sidenav.init(sidenav);

    // let dropdowns = document.querySelectorAll('.dropdown-trigger');
    //     // let dropdownInstances = M.Dropdown.init(dropdowns, {
    //     //     hover: true
    //     // });

    let fab = document.querySelectorAll('.fixed-action-btn');
    let fabInstances = M.FloatingActionButton.init(fab);

    let datepicker = document.querySelectorAll('.datepicker');
    let datepickerInstances = M.Datepicker.init(datepicker, {
        format: 'yyyy-mm-dd'
    });

    var images = document.querySelectorAll('.materialboxed');
    var imagesInstances = M.Materialbox.init(images);

    var tooltips = document.querySelectorAll('.tooltipped');
    var tooltipsInstances = M.Tooltip.init(tooltips);

    M.updateTextFields();
    // const textareas = document.querySelectorAll('textarea');
    //
    // if (textareas.length > 0) {
    //     M.textareaAutoResize(textareas);
    // }
});
